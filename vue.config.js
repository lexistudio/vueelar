module.exports = {
  outputDir: '../www',
  assetsDir: 'assets',
  css: {
    modules: false
  },
}