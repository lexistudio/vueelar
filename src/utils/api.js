// Импорт Axios
import Axios from 'axios'

// Установка url запроса по умолчанию
Axios.defaults.baseURL = 'https://jsonplaceholder.typicode.com'

// Класс инициализации запросов
export class Api {
  constructor (uri = '', params = {}) {
    this.uri = uri
    this.params = params
  }

  // GET Запрос
  get () {
    return Axios.get(this.uri, { params: this.params })
  }

  // POST Запрос
  post () {
    return Axios.post(this.uri, this.params)
  }
}

// Список запросов
export const Uri = {
  todos: '/todos'
}
