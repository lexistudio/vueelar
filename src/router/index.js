import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/template/layout/Main.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Главная',
      component: Main
    },
    {
      path: '/catalog',
      name: 'Каталог',
      params: { name: 'catalog' },
      component: () => import('@/template/layout/Work.vue'),
      children: [
        {
          path: 'admin',
          name: 'Админ',
          params: { name: 'admin' },
          component: () => import('@/template/admin/Index.vue')
        }
      ]
    }
  ]
})
