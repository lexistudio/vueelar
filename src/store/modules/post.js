import { Api, Uri } from '@/utils/api'

const state = {
  data: {}
}

const actions = {
  async addPost ({ commit }, params) {
    const obj = Object.assign({ _limit: 3 }, params)
    const resp = await new Api(Uri.todos, obj).get()
    commit('addPost', resp.data)
  }
}

const mutations = {
  addPost (state, params) {
    // eslint-disable-next-line no-return-assign
    return state.data = params
  }
}

const getters = {
  getPost (state) {
    return state.data
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}
