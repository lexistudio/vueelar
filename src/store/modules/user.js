
const state = {
  users: {}
}

const actions = {
  addUser ({ commit }, params) {
    const obj = Object.assign({ token: 'token test' }, params)
    console.log(obj)
    commit('addUser', {})
  }
}

const mutations = {
  addUser (state, list) {
    // eslint-disable-next-line no-return-assign
    return state.users = list
  }
}

const getters = {
  getUser (state) {
    return state.users
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}
