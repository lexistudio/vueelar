import Vue from 'vue'
import App from './template/App.vue'
import router from './router/'
import store from './store/'
import './css/site.styl'
import VueSimplebar from 'vue-simplebar'
import Paginate from 'vuejs-paginate'
import { loadProgressBar } from 'axios-progress-bar'
import Head from './template/layout/Head.vue'
import Menu from './template/layout/Menu.vue'
import Loader from './template/layout/Loader.vue'

Vue.use(VueSimplebar)
Vue.use(loadProgressBar)

Vue.component('Paginate', Paginate)
Vue.component('Head', Head)
Vue.component('Menu', Menu)
Vue.component('Loader', Loader)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
